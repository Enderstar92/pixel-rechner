﻿' Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

''' <summary>
''' Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
''' </summary>
Public NotInheritable Class TabVideo
    Inherits Page

    Private Sub ButtonBerechnen_Click(sender As Object, e As RoutedEventArgs) Handles ButtonBerechnen.Click
        Dim channels As Integer
        Select Case ComboBoxFarbmodell.SelectedItem.Content
            Case "Graustufen (1 Kanal)"
                channels = 1
            Case "RGB (3 Kanäle)"
                channels = 3
            Case "CMYK (4 Kanäle)"
                channels = 4
            Case Else
                channels = 0
        End Select
        Try
            TextBoxErgebnis.Text = PrettyPrintFilesize(CalculateFilesizeVideo(Convert.ToInt32(TextBoxBreitePixel.Text), Convert.ToInt32(TextBoxHoehePixel.Text), SliderFarbtiefe.Value, channels, Convert.ToDouble(TextBoxFPS.Text), ParseTime(TextBoxLaenge.Text).TotalSeconds))
        Catch ex As Exception
            TextBoxErgebnis.Text = "ungültige Eingabe"
        End Try
    End Sub
End Class
