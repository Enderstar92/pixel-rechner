﻿Module Calc
    Public Function CalculateResolution(width As Integer, height As Integer, dpi As Integer)
        Dim res(2) As Integer
        res(0) = width / 2.54 * dpi
        res(1) = height / 2.54 * dpi
        Return res
    End Function

    Public Function CalculateFilesizeImage(resWidth As Integer, resHeight As Integer, colorDepthPerChannel As Integer, channels As Integer)
        Dim filesize As Double
        filesize = resWidth * resHeight * colorDepthPerChannel / 8 * channels
        Return filesize
    End Function

    Public Function CalculateFilesizeVideo(resWidth As Integer, resHeight As Integer, colorDepthPerChannel As Integer, channels As Integer, fps As Double, lengthSeconds As Integer)
        Dim filesizeImage, filesize As Double
        filesizeImage = CalculateFilesizeImage(resWidth, resHeight, colorDepthPerChannel, channels)
        filesize = filesizeImage * fps * lengthSeconds
        Return filesize
    End Function

    Public Function PrettyPrintResolution(res As Integer())
        Dim out As String
        out = res(0) & " * " & res(1)
        Return out
    End Function

    Public Function PrettyPrintFilesize(filesize As Double)
        Dim out, unit As String
        Dim units() = {"Byte", "KiB", "MiB", "GiB", "TiB", "PiB"}
        Dim i = 0
        While filesize > 1000
            filesize /= 1024
            i += 1
        End While
        unit = units(i)
        out = filesize.ToString("###.##") & " " & unit
        Return out
    End Function

    Function ParseTime(timeAsString As String) As TimeSpan

        Dim secondsAsLong As Long
        For Each partAsString In timeAsString.Split(":")

            Dim partAsLong As Long
            Long.TryParse(partAsString, partAsLong)

            secondsAsLong *= 60
            secondsAsLong += partAsLong
        Next

        Dim timeAsTimeSpan = TimeSpan.FromSeconds(secondsAsLong)

        Return timeAsTimeSpan
    End Function
End Module
