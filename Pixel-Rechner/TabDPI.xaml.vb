﻿' Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

''' <summary>
''' Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
''' </summary>
Public NotInheritable Class TabDPI
    Inherits Page
    Private ergebnis As Integer()
    Private Sub TextBlock_SelectionChanged(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub TextBox_TextChanged(sender As Object, e As TextChangedEventArgs)

    End Sub

    Private Sub TextBox_TextChanged_1(sender As Object, e As TextChangedEventArgs)

    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Try
            ergebnis = CalculateResolution(Convert.ToInt32(textboxHeight.Text), Convert.ToInt32(textboxWidth.Text), Convert.ToInt32(textboxDPI.Text))
            TextBoxErgebnis.Text = PrettyPrintResolution(ergebnis)
        Catch ex As Exception
            TextBoxErgebnis.Text = "ungültige Eingabe"
        End Try
    End Sub

    Private Sub TextBox_TextChanged_2(sender As Object, e As TextChangedEventArgs)

    End Sub

    Private Sub ButtonDPIzuFoto_Click(sender As Object, e As RoutedEventArgs) Handles ButtonDPIzuFoto.Click
        Dim appFrame As Frame = Window.Current.Content
        Dim myMainPage As MainPage = appFrame.Content
        myMainPage.SetSelectedItem(1)
        Frame.Navigate(GetType(TabFoto), ergebnis)
    End Sub
End Class
