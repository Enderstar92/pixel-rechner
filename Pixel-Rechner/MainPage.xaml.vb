﻿' Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x407 dokumentiert.

''' <summary>
''' Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
''' </summary>
Public NotInheritable Class MainPage
    Inherits Page
    Private Sub start(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        NavigateToView("Pixel")
        SetSelectedItem(0)
    End Sub
    Public Sub SetSelectedItem(i As Integer)
        navi.SelectedItem = navi.MenuItems.Item(i)
    End Sub
    Private Sub Navi_SelectionChanged(sender As Microsoft.UI.Xaml.Controls.NavigationView, args As Microsoft.UI.Xaml.Controls.NavigationViewSelectionChangedEventArgs) Handles navi.SelectionChanged
        'Dim item = args.InvokedItemContainer
        'If item Is Nothing Then
        'Return
        'End If
        'Dim clickedView As String
        'clickedView = item.Content
        'If Not NavigateToView(clickedView) Then Return
        NavigateToView(args.SelectedItem.Content)
    End Sub

    Private Function NavigateToView(clickedView As String) As Boolean

        Select Case clickedView
            Case "Pixel"
                ContentFrame.Navigate(GetType(TabDPI), Nothing)
                Return True
            Case "Foto"
                ContentFrame.Navigate(GetType(TabFoto), Nothing)
                Return True
            Case "Video"
                ContentFrame.Navigate(GetType(TabVideo), Nothing)
                Return True
            Case Else
                Return True
        End Select
    End Function
End Class
